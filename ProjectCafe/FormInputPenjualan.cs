﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Firebase.Database;
using Firebase.Database.Query;
using System.Net;
using System.IO;

namespace ProjectCafe
{
    public partial class FormInputPenjualan : FormBase
    {
        public FormInputPenjualan()
        {
            InitializeComponent();
        }

        private void FormInputPenjualan_Load(object sender, EventArgs e)
        {
            isiMenu();

            newTransaksi();
        }

        private void newTransaksi()
        {
            lblNota.Text = DateTime.Now.ToString("yyMMddHHmmss");
            lblTanggal.Text = DateTime.Today.ToString("dd-MMMM-yyyy");
            lblOperator.Text = ClassFirebase.currentAuth.User.DisplayName;
            lblTotal.Text = "0";

        }

        async private void isiMenu()
        {
            ClassFirebase fb = new ClassFirebase();
            var result = await fb.db.Child("menu").OnceAsync<modelMenu>();

            foreach (var item in result)
            {
                var newItem = listMenu.Items.Add(item.Key);
                newItem.Text = item.Object.NamaMenu;
                newItem.SubItems.Add(item.Object.Harga.ToString());

                Image img;
                try
                {
                    //convert ke gambar
                    WebClient wc = new WebClient();
                    byte[] bytes = wc.DownloadData(item.Object.foto);
                    MemoryStream ms = new MemoryStream(bytes);
                    img = System.Drawing.Image.FromStream(ms);
                    //----
                }
                catch
                {
                    img = Properties.Resources.no_image;
                }

                imageList1.Images.Add(item.Key, img);

                newItem.ImageKey = item.Key;

            }

        }

        private void listMenu_DoubleClick(object sender, EventArgs e)
        {
            if (listMenu.SelectedItems.Count > 0)
            {
                int newRow = gridDetail.Rows.Add();
                gridDetail.Rows[newRow].Cells[0].Value = 1;
                gridDetail.Rows[newRow].Cells[1].Value = listMenu.SelectedItems[0].Text;
                gridDetail.Rows[newRow].Cells[2].Value = double.Parse(listMenu.SelectedItems[0].SubItems[1].Text);
                gridDetail.Rows[newRow].Cells[3].Value = double.Parse(listMenu.SelectedItems[0].SubItems[1].Text);
                hitungTotal();
            }
        }

        private void hitungTotal()
        {
            double total = 0;
            foreach (DataGridViewRow row in gridDetail.Rows)
            {
                total += double.Parse(row.Cells[3].Value.ToString());
            }
            lblTotal.Text = total.ToString();
        }

        async private void btnSave_Click(object sender, EventArgs e)
        {
            modelJual jual = new modelJual();
            jual.nota = lblNota.Text;
            jual.tanggal = DateTime.Parse( lblTanggal.Text);
            jual.kasir = lblOperator.Text;
            jual.details = new List<modelDetailJual>();
            foreach (DataGridViewRow row in gridDetail.Rows)
            {
                jual.details.Add(new modelDetailJual() {
                    qty = (int) row.Cells[0].Value,
                    keterangan = row.Cells[1].Value.ToString(),
                    harga = (double) row.Cells[2].Value,
                    total = (double) row.Cells[3].Value
                });
            }
            jual.total = double.Parse(lblTotal.Text);


            ClassFirebase fb = new ClassFirebase();
            await fb.db.Child("jual").PostAsync(jual);


            MessageBox.Show("wes");
        }
    }
}
