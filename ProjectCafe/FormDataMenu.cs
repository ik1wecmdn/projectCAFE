﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Firebase.Auth;
using Firebase.Database;
using Firebase.Database.Query;
using System.Net;
using System.IO;

namespace ProjectCafe
{
    public partial class FormDataMenu : FormBase
    {

        public FormDataMenu()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormInputMenu f = new FormInputMenu();
            f.Add();
            f.Show();
        }

        private void FormDataMenu_Load(object sender, EventArgs e)
        {
            loadDataMenu();
        }

        async private void loadDataMenu()
        {
            //ambil data dari firebase
            var fb = new ClassFirebase();

            ////perintah untuk mengambil data sekali
            //var result = await fb.db.Child("menu").OnceAsync<modelMenu>();

            ////loop sebanyak result
            //foreach (var item in result)
            //{
            //    //isikan ke dalam datagrid
            //    var nomor = gridMenu.Rows.Add();
            //    gridMenu.Rows[nomor].Cells[0].Value = item.Key;
            //    gridMenu.Rows[nomor].Cells[1].Value = item.Object.NamaMenu;
            //    gridMenu.Rows[nomor].Cells[2].Value = item.Object.Kategori;
            //    gridMenu.Rows[nomor].Cells[3].Value = item.Object.Harga;
            //    //convert ke gambar
            //    WebClient wc = new WebClient();
            //    byte[] bytes = wc.DownloadData(item.Object.foto);
            //    MemoryStream ms = new MemoryStream(bytes);
            //    System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
            //    //----
            //    gridMenu.Rows[nomor].Cells[4].Value = img;
            //    gridMenu.Rows[nomor].Height = 120;
            //}

            fb.db.Child("menu").AsObservable<modelMenu>().Subscribe((item) =>
            {
                if (item.Object == null)
                {
                    return;
                }

                //cek jika belum ada dalam datagrid lalu ditambahkan
                bool ketemu = false;
                int posisi = 0;
                for (int i = 0; i < gridMenu.Rows.Count; i++)
                {
                    if (gridMenu.Rows[i].Cells[0].Value.ToString() == item.Key) {
                        ketemu = true;
                        posisi = i;
                        break; //keluar dari perulangan
                    }
                }
                
                if (ketemu)
                {
                    if (item.EventType == Firebase.Database.Streaming.FirebaseEventType.Delete)
                    {
                        //hapus baris
                        this.Invoke((MethodInvoker)delegate
                        {
                            gridMenu.Rows.RemoveAt(posisi);
                        });
                    }
                    else
                    {
                        //data sudah ada di edit
                        this.Invoke((MethodInvoker)delegate
                        {
                            var nomor = posisi;
                            isiGridRow(nomor, item);
                        });
                    }
                }
                else
                {
                    //data belum ada
                    this.Invoke((MethodInvoker)delegate {
                        var nomor = gridMenu.Rows.Add();
                        isiGridRow(nomor, item);
                    });
                }

            });

        }

        void isiGridRow(int nomor, Firebase.Database.Streaming.FirebaseEvent<modelMenu> item)
        {
            gridMenu.Rows[nomor].Cells[0].Value = item.Key;
            gridMenu.Rows[nomor].Cells[1].Value = item.Object.NamaMenu;
            gridMenu.Rows[nomor].Cells[2].Value = item.Object.Kategori;
            gridMenu.Rows[nomor].Cells[3].Value = item.Object.Harga;
            Image img;
            try
            {
                //convert ke gambar
                WebClient wc = new WebClient();
                byte[] bytes = wc.DownloadData(item.Object.foto);
                MemoryStream ms = new MemoryStream(bytes);
                img = System.Drawing.Image.FromStream(ms);
                //----
            }
            catch
            {
                img = Properties.Resources.no_image;
            }
            gridMenu.Rows[nomor].Cells[4].Value = img;
            gridMenu.Rows[nomor].Height = 120;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var f = new FormInputMenu();
            f.Edit(gridMenu.CurrentRow.Cells[0].Value.ToString());
            f.Show();
        }

        async private void btnHapus_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Yakin data mau dihapus?", "^_^ Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                ClassFirebase fb = new ClassFirebase();
                await fb.db.Child("menu").Child(gridMenu.CurrentRow.Cells[0].Value.ToString()).DeleteAsync();
            }
        }
    }
}
