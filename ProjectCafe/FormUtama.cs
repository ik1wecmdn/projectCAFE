﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Firebase.Auth;

namespace ProjectCafe
{
    public partial class FormUtama : FormBase
    {
        public FormUtama()
        {
            InitializeComponent();
        }

        private void FormUtama_Load(object sender, EventArgs e)
        {
            
            labelUser.Text = ClassFirebase.currentAuth.User.DisplayName;
            picProfil.ImageLocation = ClassFirebase.currentAuth.User.PhotoUrl;
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            FormDataMenu f = new FormDataMenu();
            f.Show();
        }

        private void btnIputJual_Click(object sender, EventArgs e)
        {
            FormInputPenjualan f = new FormInputPenjualan();
            f.Show();
        }
    }
}
