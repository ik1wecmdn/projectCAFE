﻿namespace ProjectCafe
{
    partial class FormUtama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMenu = new MetroFramework.Controls.MetroTile();
            this.labelUser = new MetroFramework.Controls.MetroLabel();
            this.picProfil = new System.Windows.Forms.PictureBox();
            this.btnIputJual = new MetroFramework.Controls.MetroTile();
            ((System.ComponentModel.ISupportInitialize)(this.picProfil)).BeginInit();
            this.SuspendLayout();
            // 
            // btnMenu
            // 
            this.btnMenu.ActiveControl = null;
            this.btnMenu.Location = new System.Drawing.Point(23, 162);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(132, 135);
            this.btnMenu.TabIndex = 0;
            this.btnMenu.Text = "Data Menu";
            this.btnMenu.UseSelectable = true;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // labelUser
            // 
            this.labelUser.Location = new System.Drawing.Point(415, 107);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(138, 24);
            this.labelUser.TabIndex = 2;
            this.labelUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // picProfil
            // 
            this.picProfil.Location = new System.Drawing.Point(502, 39);
            this.picProfil.Name = "picProfil";
            this.picProfil.Size = new System.Drawing.Size(52, 63);
            this.picProfil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picProfil.TabIndex = 1;
            this.picProfil.TabStop = false;
            // 
            // btnIputJual
            // 
            this.btnIputJual.ActiveControl = null;
            this.btnIputJual.Location = new System.Drawing.Point(161, 162);
            this.btnIputJual.Name = "btnIputJual";
            this.btnIputJual.Size = new System.Drawing.Size(132, 135);
            this.btnIputJual.Style = MetroFramework.MetroColorStyle.Lime;
            this.btnIputJual.TabIndex = 3;
            this.btnIputJual.Text = "Input Penjualan";
            this.btnIputJual.UseSelectable = true;
            this.btnIputJual.UseStyleColors = true;
            this.btnIputJual.Click += new System.EventHandler(this.btnIputJual_Click);
            // 
            // FormUtama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 503);
            this.Controls.Add(this.btnIputJual);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.picProfil);
            this.Controls.Add(this.btnMenu);
            this.Name = "FormUtama";
            this.Text = "Aplikasi Cafe";
            this.Load += new System.EventHandler(this.FormUtama_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picProfil)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile btnMenu;
        private System.Windows.Forms.PictureBox picProfil;
        private MetroFramework.Controls.MetroLabel labelUser;
        private MetroFramework.Controls.MetroTile btnIputJual;
    }
}