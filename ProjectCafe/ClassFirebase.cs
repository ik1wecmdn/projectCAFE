﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Firebase.Auth;
using Firebase.Database;
using Firebase.Database.Query;
using Firebase.Storage;

namespace ProjectCafe
{
    class ClassFirebase
    {
        public static FirebaseAuth currentAuth;

        public ChildQuery db
        {
            get
            {
                FirebaseClient fbclient = new FirebaseClient("https://cafe-180aa.firebaseio.com",
                    new FirebaseOptions()
                    {
                        AuthTokenAsyncFactory = () => Task.FromResult(ClassFirebase.currentAuth.FirebaseToken)
                    });
                return fbclient.Child(ClassFirebase.currentAuth.User.LocalId);
            }
        }
        

    }

}
