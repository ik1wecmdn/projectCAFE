﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using fyiReporting;
using fyiReporting.RDL;
using fyiReporting.RdlViewer;


namespace ProjectCafe
{
    public partial class FormReportViewer : Form
    {
        public FormReportViewer()
        {
            InitializeComponent();

            DataTable data = new DataTable();
            data.Columns.Add("NAMA", typeof(string));
            data.Columns.Add("KATEGORI", typeof(string));
            data.Columns.Add("HARGA", typeof(Int32));

            data.Rows.Add(new object[] { 
                "ES TEH", "MINUMAN", 5000
            });

            

            RdlViewer rdlView = new RdlViewer();
            rdlView.SourceFile = new Uri( Application.StartupPath + "\\reports\\menu.rdl");
            rdlView.Report.DataSets["Data"].SetData(data);
            rdlView.Rebuild();

            rdlView.Dock = DockStyle.Fill;
            Controls.Add(rdlView);

        }
    }
}
