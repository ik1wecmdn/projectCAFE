﻿namespace ProjectCafe
{
    partial class FormInputMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInputMenu));
            this.txtNama = new MetroFramework.Controls.MetroTextBox();
            this.txtKategori = new MetroFramework.Controls.MetroTextBox();
            this.txtHarga = new MetroFramework.Controls.MetroTextBox();
            this.picMenu = new System.Windows.Forms.PictureBox();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNama
            // 
            // 
            // 
            // 
            this.txtNama.CustomButton.Image = null;
            this.txtNama.CustomButton.Location = new System.Drawing.Point(287, 1);
            this.txtNama.CustomButton.Name = "";
            this.txtNama.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNama.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNama.CustomButton.TabIndex = 1;
            this.txtNama.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNama.CustomButton.UseSelectable = true;
            this.txtNama.CustomButton.Visible = false;
            this.txtNama.Lines = new string[0];
            this.txtNama.Location = new System.Drawing.Point(45, 121);
            this.txtNama.MaxLength = 32767;
            this.txtNama.Name = "txtNama";
            this.txtNama.PasswordChar = '\0';
            this.txtNama.PromptText = "Nama Menu :";
            this.txtNama.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNama.SelectedText = "";
            this.txtNama.SelectionLength = 0;
            this.txtNama.SelectionStart = 0;
            this.txtNama.ShortcutsEnabled = true;
            this.txtNama.Size = new System.Drawing.Size(309, 23);
            this.txtNama.TabIndex = 0;
            this.txtNama.UseSelectable = true;
            this.txtNama.WaterMark = "Nama Menu :";
            this.txtNama.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNama.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtKategori
            // 
            // 
            // 
            // 
            this.txtKategori.CustomButton.Image = null;
            this.txtKategori.CustomButton.Location = new System.Drawing.Point(133, 1);
            this.txtKategori.CustomButton.Name = "";
            this.txtKategori.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtKategori.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtKategori.CustomButton.TabIndex = 1;
            this.txtKategori.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtKategori.CustomButton.UseSelectable = true;
            this.txtKategori.CustomButton.Visible = false;
            this.txtKategori.Lines = new string[0];
            this.txtKategori.Location = new System.Drawing.Point(45, 150);
            this.txtKategori.MaxLength = 32767;
            this.txtKategori.Name = "txtKategori";
            this.txtKategori.PasswordChar = '\0';
            this.txtKategori.PromptText = "Kategori :";
            this.txtKategori.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtKategori.SelectedText = "";
            this.txtKategori.SelectionLength = 0;
            this.txtKategori.SelectionStart = 0;
            this.txtKategori.ShortcutsEnabled = true;
            this.txtKategori.Size = new System.Drawing.Size(155, 23);
            this.txtKategori.TabIndex = 1;
            this.txtKategori.UseSelectable = true;
            this.txtKategori.WaterMark = "Kategori :";
            this.txtKategori.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtKategori.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtHarga
            // 
            // 
            // 
            // 
            this.txtHarga.CustomButton.Image = null;
            this.txtHarga.CustomButton.Location = new System.Drawing.Point(133, 1);
            this.txtHarga.CustomButton.Name = "";
            this.txtHarga.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtHarga.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtHarga.CustomButton.TabIndex = 1;
            this.txtHarga.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtHarga.CustomButton.UseSelectable = true;
            this.txtHarga.CustomButton.Visible = false;
            this.txtHarga.Lines = new string[0];
            this.txtHarga.Location = new System.Drawing.Point(45, 179);
            this.txtHarga.MaxLength = 32767;
            this.txtHarga.Name = "txtHarga";
            this.txtHarga.PasswordChar = '\0';
            this.txtHarga.PromptText = "Harga Jual :";
            this.txtHarga.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtHarga.SelectedText = "";
            this.txtHarga.SelectionLength = 0;
            this.txtHarga.SelectionStart = 0;
            this.txtHarga.ShortcutsEnabled = true;
            this.txtHarga.Size = new System.Drawing.Size(155, 23);
            this.txtHarga.TabIndex = 2;
            this.txtHarga.UseSelectable = true;
            this.txtHarga.WaterMark = "Harga Jual :";
            this.txtHarga.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtHarga.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // picMenu
            // 
            this.picMenu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picMenu.Location = new System.Drawing.Point(258, 150);
            this.picMenu.Name = "picMenu";
            this.picMenu.Size = new System.Drawing.Size(96, 95);
            this.picMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMenu.TabIndex = 3;
            this.picMenu.TabStop = false;
            this.picMenu.DoubleClick += new System.EventHandler(this.picMenu_DoubleClick);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(289, 310);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 36);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseSelectable = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(218, 310);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(65, 36);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(10, 63);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(366, 297);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // FormInputMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 395);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.picMenu);
            this.Controls.Add(this.txtHarga);
            this.Controls.Add(this.txtKategori);
            this.Controls.Add(this.txtNama);
            this.Name = "FormInputMenu";
            this.Text = "Form Input Menu";
            ((System.ComponentModel.ISupportInitialize)(this.picMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txtNama;
        private MetroFramework.Controls.MetroTextBox txtKategori;
        private MetroFramework.Controls.MetroTextBox txtHarga;
        private System.Windows.Forms.PictureBox picMenu;
        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Controls.MetroButton btnSave;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}