﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Firebase.Auth;

namespace ProjectCafe
{
    public partial class FormLogin : FormBase
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            FormRegister f = new FormRegister();
            f.Show();
        }

        async private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                var authProv = new FirebaseAuthProvider(
                    new FirebaseConfig("AIzaSyBM7VG-ql3JRSWZdwBx6YbR2qgjgiFi9Ks"));
                var authLink = await authProv.SignInWithEmailAndPasswordAsync(
                    txtEmail.Text, txtPassword.Text);
                //MessageBox.Show("Selamat datang " + authLink.User.DisplayName);
                ClassFirebase.currentAuth = authLink;
                FormUtama f = new FormUtama();
                f.Show();
                f.FormClosed += (ss, ee) => {
                    txtEmail.Clear();
                    txtPassword.Clear();
                    Show();
                };
                Hide();
            }
            catch (FirebaseAuthException ex)
            {
                if (ex.Reason == AuthErrorReason.UnknownEmailAddress)
                {
                    MessageBox.Show("Email tidak terdaftar");
                }
                else if (ex.Reason == AuthErrorReason.WrongPassword)
                {
                    MessageBox.Show("Username atau Password salah... cek kembali...");
                }
                else
                {
                    MessageBox.Show("ERROR LOGIN cek koneksi internet");
                }
            }

        }
    }
}
