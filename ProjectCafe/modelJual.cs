﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCafe
{
    class modelJual
    {
        public string nota { get; set; }
        public DateTime tanggal { get; set; }
        public string kasir { get; set; }
        public double total { get; set; }
        public List<modelDetailJual> details { get; set; }
    }

    class modelDetailJual
    {
        public int qty { get; set; }
        public string keterangan { get; set; }
        public double harga { get; set; }
        public double total { get; set; }
    }
}
