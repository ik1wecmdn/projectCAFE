﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectCafe
{
    public partial class textAngka : MetroFramework.Controls.MetroTextBox
    {
        public textAngka()
        {
            InitializeComponent();
        }

        private void textAngka_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void textAngka_TextChanged(object sender, EventArgs e)
        {
            string harga = this.Text.Replace("Rp ", "");
            if (harga != "")
            {
                //format rupiah
                this.Text = string.Format("Rp {0:n0}",
                    double.Parse(harga));
                //biar kursor selalu di akhir
                this.SelectionStart = this.Text.Length;
            }
            else
            {
                this.Text = "";
            }
        }
    }
}
