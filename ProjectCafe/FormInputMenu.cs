﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using Firebase.Database;
using Firebase.Database.Query;
using Firebase.Storage;
using System.Net;

namespace ProjectCafe
{
    public partial class FormInputMenu : FormBase
    {
        string keyEdit = "";
        modelMenu m ;

        public FormInputMenu()
        {
            InitializeComponent();
        }

        async private void btnSave_Click(object sender, EventArgs e)
        {
            
            m.NamaMenu = txtNama.Text;
            m.Kategori = txtKategori.Text;
            m.Harga = double.Parse(txtHarga.Text);

            //MemoryStream stream = new MemoryStream();
            //picMenu.Image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);

            if (picMenu.ImageLocation != null)
            {
                var stream = File.Open(picMenu.ImageLocation, FileMode.Open);

                //proses upload foto
                FirebaseStorage fbstorage = new FirebaseStorage("cafe-180aa.appspot.com",
                    new FirebaseStorageOptions()
                    {
                        AuthTokenAsyncFactory = () => Task.FromResult(ClassFirebase.currentAuth.FirebaseToken)
                    });
                var hasilUpload = await fbstorage
                    .Child(ClassFirebase.currentAuth.User.LocalId)
                    .Child("FotoMenu/" + txtNama.Text + ".jpg")
                    .PutAsync(stream);
                m.foto = hasilUpload;
            }
           
            //------------------

            //proses simpan data
            //FirebaseClient fbclient = new FirebaseClient("https://cafe-180aa.firebaseio.com",
            //    new FirebaseOptions()
            //    {
            //        AuthTokenAsyncFactory = () => Task.FromResult(ClassFirebase.currentAuth.FirebaseToken)
            //    });
            //await fbclient.Child(ClassFirebase.currentAuth.User.LocalId).Child("menu").PostAsync(m);

            var fb = new ClassFirebase();
            if (keyEdit == "")
            {
                //add data
                await fb.db.Child("menu").PostAsync(m);
            }
            else
            {
                //edit data
                await fb.db.Child("menu").Child(keyEdit).PutAsync(m);
            }

            MessageBox.Show("wes");
        }

        private void picMenu_DoubleClick(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "IMage | *.jpg;*.png";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                picMenu.ImageLocation = ofd.FileName;
            }
        }

        async internal void Edit(string key)
        {
            //buat status edit
            keyEdit = key;

            ClassFirebase fb = new ClassFirebase();
            //ambil data satu item saja
            m = await fb.db.Child("menu").Child(key).OnceSingleAsync<modelMenu>();
            //tampilkan ke dalam form
            txtNama.Text = m.NamaMenu;
            txtKategori.Text = m.Kategori;
            txtHarga.Text = m.Harga.ToString();

            //convert ke gambar
            Image img;
            try
            {
                WebClient wc = new WebClient();
                byte[] bytes = wc.DownloadData(m.foto);
                MemoryStream ms = new MemoryStream(bytes);
                img = System.Drawing.Image.FromStream(ms);
            }
            catch
            {
                img = null;
            }

            picMenu.Image = img;

            pictureBox1.Hide();

        }

        internal void Add()
        {
            m = new modelMenu();

            pictureBox1.Hide();
        }
    }
}
