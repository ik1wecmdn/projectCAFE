﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Firebase.Auth;

namespace ProjectCafe
{
    public partial class FormRegister : FormBase
    {
        public FormRegister()
        {
            InitializeComponent();
        }

        async private void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                var authProv = new FirebaseAuthProvider(
                    new FirebaseConfig("AIzaSyBM7VG-ql3JRSWZdwBx6YbR2qgjgiFi9Ks"));
                await authProv.CreateUserWithEmailAndPasswordAsync(
                    txtEmail.Text, txtPassword.Text, txtFullname.Text);
                MessageBox.Show("Register berhasil, silahkan login menggunakan email dan password anda..");
            }
            catch (FirebaseAuthException ex)
            {
                if (ex.Reason == AuthErrorReason.EmailExists)
                {
                    MessageBox.Show("Email sudah ada...");
                }
                else if (ex.Reason == AuthErrorReason.InvalidEmailAddress)
                {
                    MessageBox.Show("Email gak bener..");
                }
                else if (ex.Reason == AuthErrorReason.WeakPassword)
                {
                    MessageBox.Show("Password jelek...");
                }
                else
                {
                    MessageBox.Show("ERROR TIDAK DIKETAHUI", "^_^ ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
